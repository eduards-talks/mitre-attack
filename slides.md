<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->

## Mitre Att&ck
<!-- .element: class="no-toc-progress" --> <!-- slide not in toc progress bar -->

----

## System Owner/User Discovery (T1033)

$whoami

- Eduard Thamm
- Prisma Capacity
- Engineer with an interest in security
- Public speaker
- Cookie monster

----

# Preface

_It does not matter who you are or where you are. You can make security better._

----

## Tough Questions

- How good are my defenses
- Do I have a change to detect the threat de jour
- Is the way I invest my time useful
- Is the money I invest spent wisely

----

## What is Mitre Att&ck?

- Knowledge base of adversary behavior
- Based on real-world observations
- Free, open and globally accessible
- Community driven
- Common Terminology

----

## Who is it from?

- Mitre Corp.
- Other Orgs.
- Individual Contributors

----  ----

# A short Intro

----

## Detecting TTPs

<img src="/images/pyramid_of_pain.png" height="480px" >

Note:
Tactic - highest level of behavior description
Techniques - detailed behavior description
Procedure - low level highly detailed description

----

## Adversary Life Cycle

<img src="/images/adversary_lc.png" height="480px" >

----  ----

## Breaking Down ATT&CK

So how does the matrix work?

Note:

Switch to real world demo here

----

<img src="/images/ma_tt.png" height="480px" >

----

<img src="/images/ma_p.png" height="480px" >


----

## Example Technique

<img src="/images/ma_technique_detail.png" height="480px" >

----

## Groups

<img src="/images/ma_group.png" height="480px" >

----  ----

# Use Cases

- Detection (also see Mitre D3fend, DeTT&CT Framework)
- Assessment and Engineering
- Threat Intelligence
- Adversary Emulation

----  ----

## Detection Level 1

- Behavioral Analytics (Move from Pyramid of Pain)
- Adapt those to your environment
- Resources (google those):
  - Cyber Analytics Repository
  - Endgame EQL Analytics Library
  - Threat Hunter Playbook
  - Sigma
  - Atomic Threat Coverage
  - ...

----

## CAR Example

<img src="/images/car_example.png" height="480px" >


----

## Detection Level 2

- Look at what techniques you can detect with what you already have
- Host based data is useful, but consider network data as well
- Example data sources:
  - Windows Registry
  - Process monitoring
  - CLI parameters
  - NIDS
  - ...

----

## But how

- Choose one data source and write your own analytics
- Mitre provides some scripts to help you get started

----

## Detection Level 3

- Assess your detection coverage across ATT&CK
  - Start with one tactic and expand from there
  - Choose a 'coverage rating' that works for you:
    - 1-5
    - Low, Medium, High
    - Remember 'perfect' coverage is not real!
  - Check out Att&ck Navigator

----  ----

## Assessment and Engineering

- Derive decisions about what to collect (and buy)
  - Gaps
  - Effective defenses
  - Tooling
- Help move towards a broader view of security beyond detection
- Increase awareness of where you (may need to) accept risk

----

## A&E Level 1

- Collect one log source
- Places to start
  - System Event logs
  - Sysmon

----

## A&E Level 2

- Think and assess beyond detection
- What can you mitigate
  - Remember it is People, Process, Technology
  - Mitigate with policies
  - Mitigate with Tooling
- Risk acceptance

----

## But how

- Spearphishing Attachments
  - awareness trainings
- Supply Chain Compromise
  - Maybe you do not have the resources to deal with that. Have a honest talk, accept the risk and move on.

----

## A&E Level 3

- Plan out tooling and log acquisition strategy based on coverage
- Determine the techniques you already detect from your sources
- Consider the changes you could make
  - Config changes
  - Tooling changes
- Examine budget and plan resource usage

----  ----

## Threat Intelligence

- Use knowledge of adversary behavior to help inform defense
- Structuring threat intelligence with Att&ck:
  - Compare behaviors
  - Communicate in a common language

----

## TI Level 1

- Choose 1 threat group you care about
- Look at existing techniques this group uses
  - https://attack.mitre.org/groups
- Base recommendations on that
  - e.g. File types used by Cobalt Group for Spearphishing

----

## TI Level 2

- Map your own TI to Att&ck
  - Use e.g. incident write-ups
- Consider how you store intelligence
- Start with tactics
- This is a learning experience, work as a team
- Use it to adapt your defenses based on adversaries that target you

----

## TI Level 3

- Map more of your own TI to Att&ck
  - TI subscriptions
  - Real-Time Alerts
  - ...
- Prioritize frequently used techniques
  - Yes there is bias in the data
  - This prioritizes known over unknown adversary behavior
- Share your intel

----

## Top 20 by Mitre

<img src="/images/top_20_uc.png" height="480px" >



----  ----

## Adversary Emulation

- Basically about testing your detections/mitigations
- Use Att&ck to organize the red team plans
  - Focus on behaviors
  - Subset of threat-based security testing

----

## AE Level 1

- No red team?
- Try it yourself (automated tooling)
  - Caldera (Mitre)
  - Red Team Automation (RTA for short)
  - Metta (Uber)
- Red Canary's Atomic Red Team

----

## AE Level 2

- Use Att&ck to improve your red team
  - Use different techniques
  - Discuss how different techniques change outcomes
  - Work towards purple
- APT3 Adversary Emulation Plan
  - Create Field Manuals

----

## AE Level 3

- Develop your own AE plan
  - Customize to adversaries important to you
- Leverage Att&ck for communication with defenders
- Mitre Resources
  - Att&ck Evaluation Methodology
  - Threat-based Purple Teaming with Att&CK
  - Att&cking the Status Quo: Threat-based Adversary Emulation with Mitre Att&ck

----  ----

## All together now

- TI: What techniques do our adversaries use?
  - Defenders: What can we detect?
  - Assessment: How can we improve?
  - Red: Does this hold up in the real world?
- You end up with a clear picture on the matrix of where you want to focus your efforts


----

## Wrapping up

- Att&ck is a tool that can help you no matter where you are on your journey
- Do what you can, with what you have, where you are
- Choose a starting point that works for you

----  ----

## Thank you for your time
